-- chikun :: 2016
-- Twee file importer

local twee_importer = { }


function twee_importer:loadFile(file_name)

	-- Quit function if file does not exist
	if (not love.filesystem.exists(file_name)) then return nil end


	local import_data = { }

	local current_passage = { }

	-- Iterate through all lines
	for line in love.filesystem.lines(file_name) do

		-- Determines new passages
		if (line:starts_with(":: ")) then

			-- Create new table in import_data
			import_data[line:sub(4)] = {
				choices = { },
				text = ""
			}

			-- Set current_passage
			current_passage = import_data[line:sub(4)]

		-- Determines choices
		elseif (line:starts_with("[[")) then

			-- Split choice text by title and link
			local parts = line:sub(3, -3):split("|")

			-- Add choice to the correct table
			table.insert(current_passage.choices, {
					text = parts[1],
					link = parts[2] or parts[1]
				})

		-- Determines normal lines
		else

			if (current_passage.text:len() > 0) then

				current_passage.text = current_passage.text .. " \n "
			end

			current_passage.text = current_passage.text .. line
		end
	end

	-- Delete trailing empty lines
	for i = #import_data, 1, -1 do

		if (import_data[i] == " \n ") then

			table.remove(import_data, i)
		else

			break
		end
	end

	import_data.current = "Iq"

	return import_data
end





return twee_importer
