-- chikun :: 2016
-- Extra string functions

local string_functions = { }


function string.starts_with(text, sub)

   return string.sub(text, 1, string.len(sub)) == sub
end


function string.ends_with(text, sub)

   return (sub == '') or (string.sub(text, -string.len(sub)) == sub)
end


function string.split(text, sub)

	-- If no sub given, will split by whitespace
	sub = sub or "%s"

	-- Temporary table for insertion
	local split_table = { }

	-- Iterate through all text
	for str in string.gmatch(text, "([^" .. sub .. "]+)") do

		table.insert(split_table, str)
	end

	-- Return table of split values
	return split_table
end


return string_functions
