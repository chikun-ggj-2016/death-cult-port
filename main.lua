-- chikun :: 2016
-- Main game loop

require "lua"

local data = ct:loadFile("story.tw")

if (love.system.getOS() == "Android") then

	jit.on()
end


function love.load()

	text_height, text_y = 0, 0

	m = lg.getWidth() * 0.05

	theme = {
		bg = {
			255, 255, 255
		},
		text = {
			0, 0, 0
		},
		choice = {
			230, 70, 70
		}
	}
end

function love.update(dt)

	if (lm.isDown(1) and mouse_y) then

		text_y = math.clamp(0, text_y + (mouse_y - lm.getY()),
							text_height - getYStart())

		mouse_y = lm.getY()
	end
end

function love.mousepressed(x, y, mb)

	if (lm.getY() < getYStart()) then

		mouse_y = y
	else

		local mouse = { x = x, y = y, w = 0, h = 0 }

		for key, value in ipairs(getChoiceBoxes()) do

			if (math.doesCollide(value, mouse)) then

				data.current = data[data.current].choices[key].link
				text_y = 0

				break
			end
		end
	end
end

function love.mousereleased(x, y, mb) mouse_y = nil end

function love.resize(w, h)

	m = lg.getWidth() * 0.05

	cf:resize(w)
end


function love.draw()

	lg.setBackgroundColor(theme.bg)

	local font = lg.getFont()

	lg.setColor(theme.text)

	lg.setFont(cf.text.normal)
	lg.printText(data[data.current].text, m, m, m * 18)

	local choice_boxes = getChoiceBoxes()

	for key, choice in ipairs(data[data.current].choices) do

		local box = choice_boxes[key]
		local x, y, w, h = box.x, box.y, box.w, box.h

		local tmp, wrapped = font:getWrap(choice.text, w)
		local font_yoff = #wrapped * font:getHeight() / 2

		lg.setColor(theme.bg)
		lg.rectangle('fill', x - m, y - m, w + m * 2, h + m * 2)

		lg.setColor(theme.choice)
		lg.rectangle('fill', x, y, w, h)

		lg.setColor(theme.text)
		lg.printf(choice.text, x, y + h / 2 - font_yoff, w, 'center')
	end
end


function getChoiceBoxes()

	local y_start = getYStart()

	local choice_boxes = { }

	for i = 1, #data[data.current].choices do

		table.insert(choice_boxes, {
				x = m,
				y = y_start + m + (i - 1) * m * 4,
				w = m * 18,
				h = m * 3
			})
	end

	return choice_boxes
end


function getYStart()

	return lg.getHeight() - m * 4 * #data[data.current].choices - m
end


function lg.printText(text, x, y, w)

	local dx, dy, font = 0, -text_y, lg.getFont()

	local bold, italic = false, false

	local space_width = font:getWidth(" ")

	local skipLine = function()

		dx = 0
		dy = dy + font:getHeight()
	end

	for word in text:gmatch("([^ ]+)") do

		if (word:find("<i>")) then

			word = word:gsub("<i>", "")
			italic = true
		end

		setTextFont(bold, italic)

		if (word:find("</i>")) then

			word = word:gsub("</i>", "")
			italic = false
		end

		font = lg.getFont()

		if (word ~= "\n") then

			local word_width = font:getWidth(word)

			if (dx + word_width > w) then

				skipLine()
			end

			lg.print(word, x + dx, y + dy)

			dx = dx + word_width + space_width

		else

			skipLine()
		end
	end

	-- Add one extra line and update text_height
	skipLine() ; text_height = dy + text_y
end


function setTextFont(bold, italic)

	local fnt = cf.text.normal

	if (italic) then

		fnt = cf.text.italic
	elseif (bold) then

		fnt = cf.text.bold
	end

	lg.setFont(fnt)
end
