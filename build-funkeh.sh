#!/bin/sh
# Josef's custom build script for death-cult-wrapper

# Exports needed
export ANDROID_HOME=/opt/android-sdk-linux
export ANDROID_NDK=/opt/android-ndk-r9c
export ANDROID_SDK=${ANDROID_HOME}
export ANDROID_SWT=/usr/share/java
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/
export PATH=$PATH:$ANDROID_SDK/tools:$ANDROID_NDK
export ZIP_PATH=/home/funkeh/Repos/chikun-ggj-2016/death-cult-wrapper/assets/

# Compress game to a .love file
cd ~/Repos/chikun-ggj-2016/death-cult-port
git archive --format=zip master -o game.love
mv game.love $ZIP_PATH

# Create the debug version of the game
cd ~/Repos/chikun-ggj-2016/death-cult-wrapper
ant debug
