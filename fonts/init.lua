-- chikun :: 2016
-- Load all fonts for game


local current_dir, fonts = ..., { }

function fonts:resize(w)

	local lg, multi = lg or love.graphics, w / 480

	-- Calculate regular text size
	local text_size = math.ceil(28 * multi)

	self.text = {
		normal = lg.newFont(current_dir .. "/Neuton-Regular.ttf", text_size),
		bold =   lg.newFont(current_dir .. "/Neuton-Bold.ttf", text_size),
		italic = lg.newFont(current_dir .. "/Neuton-Italic.ttf", text_size)
	}
end

fonts:resize(love.graphics.getWidth())

return fonts
